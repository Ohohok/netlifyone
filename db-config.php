<?php

/**
 * LudicrousDB configuration file
 *
 * This file should be copied to ABSPATH/db-config.php and modified to suit your
 * database environment. This file comes with a basic configuration by default.
 *
 * See README.md for documentation.
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * charset (string)
 * This sets the default character set. Since WordPress 4.2, the suggested
 * setting is "utf8mb4". We strongly recommend not downgrading to utf8,
 * using latin1, or sticking to the default: utf8mb4.
 *
 * Default: utf8mb4
 */
$wpdb->charset = 'utf8mb4';

/**
 * collate (string)
 * This sets the default column collation. For best results, investigate which
 * collation is recommended for your specific character set.
 *
 * Default: utf8mb4_unicode_520_ci
 */
$wpdb->collate = 'utf8mb4_unicode_520_ci';

/**
 * save_queries (bool)
 * This is useful for debugging. Queries are saved in $wpdb->queries. It is not
 * a constant because you might want to use it momentarily.
 * Default: false
 */
$wpdb->save_queries = false;

/**
 * recheck_timeout (float)
 * The amount of time to wait before trying again to ping mysql server.
 *
 * Default: 0.1 (Seconds)
 */
$wpdb->recheck_timeout = 0.1;

/**
 * persistent (bool)
 * This determines whether to use mysql_connect or mysql_pconnect. The effects
 * of this setting may vary and should be carefully tested.
 * Default: false
 */
$wpdb->persistent = false;

/**
 * allow_bail (bool)
 * This determines whether to use mysql connect or mysql connect has failed and to bail loading the rest of WordPress
 * Default: false
 */
$wpdb->allow_bail = false;

/**
 * max_connections (int)
 * This is the number of mysql connections to keep open. Increase if you expect
 * to reuse a lot of connections to different servers. This is ignored if you
 * enable persistent connections.
 * Default: 10
 * 
 * Strattic: set to 2 so sites don't hit db connection limit, will be increased in the future
 */
$wpdb->max_connections = 2;

/**
 * check_tcp_responsiveness
 * Enables checking TCP responsiveness by fsockopen prior to mysql_connect or
 * mysql_pconnect. This was added because PHP's mysql functions do not provide
 * a variable timeout setting. Disabling it may improve average performance by
 * a very tiny margin but lose protection against connections failing slowly.
 * Default: true
 */
$wpdb->check_tcp_responsiveness = true;

$wpdb->reconnect_retries = 5;

/**
 * The cache group that is used to store TCP responsiveness.
 * Default: ludicrousdb
 */
$wpdb->cache_group = 'ludicrousdb';

$db_host = getenv('WORDPRESS_DB_HOST') ?: DB_HOST;
$db_host_ro = getenv('WORDPRESS_DB_HOST_RO') ?: ( defined( DB_HOST_RO ) ? DB_HOST_RO : null );

$db_user = getenv('WORDPRESS_DB_USER') ?: DB_USER;
$db_pass = getenv('WORDPRESS_DB_PASSWORD') ?: DB_PASSWORD;
$db_name = getenv('WORDPRESS_DB_NAME') ?: DB_NAME;

$main_db_config = array(
  'host'     => $db_host,     // If port is other than 3306, use host:port.
  'user'     => $db_user,
  'password' => $db_pass,
  'name'     => $db_name,
  'write'    => 1,
  'read'     => 2, // priority 2 as fallback
  'timeout'  => 1
);
$wpdb->add_database($main_db_config);

if ($db_host_ro && !is_admin()) {
  /**
   * This adds the read replica config
   */
  $wpdb->add_database(array(
    'host'     => $db_host_ro,
    'user'     => $db_user,
    'password' => $db_pass,
    'name'     => $db_name,
    'write'    => 0,
    'read'     => 1,
    'timeout'  => 1
  ));
}

if ( WP_DEBUG && WP_DEBUG_DISPLAY ) {
  $wpdb->show_errors();
}

function handle_db_error($dbhost, $port, $operation, $table, $dataset, $dbhname, $wpdb) {
  wp_load_translations_early();

  // Load custom DB error template, if present.
  if ( file_exists( WP_CONTENT_DIR . '/db-error.php' ) ) {
    require_once WP_CONTENT_DIR . '/db-error.php';
    die();
  }

  $message = '<h1>' . __( 'Error establishing a database connection' ) . "</h1>\n";

  $message .= '<p>' . sprintf(
    /* translators: 1: wp-config.php, 2: Database host. */
    __( 'This either means that the username and password information in your %1$s file is incorrect or we can&#8217;t contact the database server at %2$s. This could mean your host&#8217;s database server is down.' ),
    '<code>wp-config.php</code>',
    '<code>' . htmlspecialchars( $dbhost, ENT_QUOTES ) . '</code>'
  ) . "</p>\n";

  $message .= "<ul>\n";
  $message .= '<li>' . __( 'Are you sure you have the correct username and password?' ) . "</li>\n";
  $message .= '<li>' . __( 'Are you sure you have typed the correct hostname?' ) . "</li>\n";
  $message .= '<li>' . __( 'Are you sure the database server is running?' ) . "</li>\n";
  $message .= "</ul>\n";

  $message .= '<p>' . sprintf(
    /* translators: %s: Support forums URL. */
    __( 'If you&#8217;re unsure what these terms mean you should probably contact your host. If you still need help you can always visit the <a href="%s">WordPress Support Forums</a>.' ),
    __( 'https://wordpress.org/support/forums/' )
  ) . "</p>\n";
  $wpdb->bail( $message, 'db_connect_fail' );
  // Call dead_db() if bail didn't die, because this database is no more.
  // It has ceased to be (at least temporarily).
  dead_db();
}
$wpdb->add_callback('handle_db_error', 'db_connection_error');
